from abc import ABC, abstractclassmethod

class Person(ABC):

    @abstractclassmethod
    def getFullName(self):
        pass

    @abstractclassmethod
    def addRequest(self):
        pass

    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass

class Employee(Person):

    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department

    def setFirstName(self):
        self.firstName

    def setLastName(self):
        self.lastName

    def setEmail(self):
        self.email

    def setDepartment(self):
        self.department

    def getFirstName(self):
        return f"{self.firstName}"

    def getLastName(self):
        return f"{self.lastName}"

    def getEmail(self):
        return f"{self.email}"

    def getDepartment(self):
        return f"{self.department}"

    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def addRequest(self):
        return "Request has been added"

    def getFullName(self):
        return f"{self.firstName} {self.lastName}"

    def login(self):
        return f"{self.email} has logged in"

    def logout(self):
        return f"{self.email} has logged out"

class TeamLead(Person):

    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department
        self.members = []
      

    def addUser(self):
        pass

    def addRequest(self):
        self.addRequest = addRequest
        return "New user added"

    def checkRequest(self):
        self.checkRequest = checkRequest
        return "Request has been added"

    def getFullName(self):
        return f"{self.firstName} {self.lastName}"

    def addMember(self, members):
        self.members.append(members)
        return "Request has been added"

    def get_members(self):
         return self.members


class Admin(Person):

    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email 
        self.department = department

    def addUser(self):
        return "User has been added"

    def addRequest(self):
        pass

    def checkRequest(self):
        pass

    def getFullName(self):
        return f"{self.firstName} {self.lastName}"




class Request():

    def __init__(self, name, requester, dateRequested):
        self.name = name 
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = []

    def set_status(self, status):
        return self.status
    
    def updateRequest(self):
        return f"Request {self.name} has been updated"

    def closeRequest(self):
        return f"Request {self.name} has been closed"

    def cancelRequest(self):

        return f"Request {self.name} has been cancelled"
     



# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
